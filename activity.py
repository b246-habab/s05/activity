# Specifications
# 1. Create a Person class that is an abstract class that has the following methods
#   a. getFullName method
#   b. addRequest method
#   c. checkRequest method
#   d. addUser method

from abc import ABC, abstractclassmethod

class Person(ABC):
    @abstractclassmethod
    def getFullName(self):
        pass
    def addRequest(self):
        pass
    def checkRequest(self, request):
        pass
    def addUser(self, user):
        pass
    
# 2. Create an Employee class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department
#   b. Methods (Don't forget getters and setters)
#   c. Abstract methods
#       i. checkRequest() - placeholder method (For the checkRequest and addUser methods, they should do nothing, for the functions that does nothing, 'pass' keyword will be a great help)
#       ii. addUser  - placeholder method (For the checkRequest and addUser methods, they should do nothing, for the functions that does nothing, 'pass' keyword will be a great help)
#       iii. addRequest - returns "Request has been added"
#       iv. getFullName - returns both object's firstname and lastname through self
#   d. Custom Methods
#       v. login() - returns "<Email> has logged in"
#       vi. logout() - returns "<Email> has logged out"


class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.department = department
        
    #setters
    def set_firstName(self, firstName):
        self.firstName = firstName
    def set_lastName(self, lastName):
        self.lastName = lastName
    def set_email(self, email):
        self.email = email
    def set_department(self, department):
        self.department = department
    
    #getters
    def get_firstName(self):
        return(self.firstName)
    def get_lastName(self):
        return(self.lastName)
    def get_email(self):
        return(self.email)
    def get_department(self):
        return(self.department)
    def getFullName(self):
        return(f"{self.firstName} {self.lastName}")
    
    #Methods
    def login(self):
        return(f"{self.email} has logged in")
    def logout(self):
        return(f"{self.email} has logged out")
    def addRequest(self):
        return("Request has been added")
    def checkRequest(self, request):
        return("Request has been checked")
    
# 3. Create a TeamLead class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department, members
#   b. Methods
#       Abstract methods
#       For the addRequest and addUser methods, they should do nothing.
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       addMember() - adds an employee to the members list
#       Note: All methods just return Strings of simple text
#           ex. Request has been added


class teamLead(Person):
    def __init__(self, firstName, lastName, email, department, members=[]):
        super().__init__()
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.department = department
        self.members = members
    #setters
    def set_firstName(self, firstName):
        self.firstName = firstName
    def set_lastName(self, lastName):
        self.lastName = lastName
    def set_email(self, email):
        self.email = email
    def set_department(self, department):
        self.department = department
    def set_members(self, members):
        self.members = members
    
    #getters
    def get_firstName(self):
        return(self.firstName)
    def get_lastName(self):
        return(self.lastName)
    def get_email(self):
        return(self.email)
    def get_department(self):
        return(self.department)
    def get_members(self):
        return(self.members)
    #Methods
    def getFullName(self):
        return(f"{self.firstName} {self.lastName}")
    def login(self):
        return(f"{self.email} has logged in")
    def logout(self):
        return(f"{self.email} has logged out")
    def addMember(self, employee):
        return self.members.append(employee)
        

# 4. Create an Admin class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department
#   b. Methods
#       Abstract methods
#       For the checkRequest and addRequest methods, they should do nothing.
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       addUser() - outputs "New user added"
#       Note: All methods just return Strings of simple text
#           ex. Request has been added


class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.department = department
    #setters
    def set_firstName(self, firstName):
        self.firstName = firstName
    def set_lastName(self, lastName):
        self.lastName = lastName
    def set_email(self, email):
        self.email = email
    def set_department(self, department):
        self.department = department
    #getters
    def get_firstName(self):
        return(self.firstName)
    def get_lastName(self):
        return(self.lastName)
    def get_email(self):
        return(self.email)
    def get_department(self):
        return(self.department)
    # Methods
    def getFullName(self):
        return(f"{self.firstName} {self.lastName}")
    def login(self):
        return(f"{self.email} has logged in")
    def logout(self):
        return(f"{self.email} has logged out")
    def addUser(self):
        return("User has been added")
        
        
# 5. Create a Request that has the following properties and methods
#   a. properties
#       name, requester, dateRequested, status
#   b. Methods
#       updateRequest
#       closeRequest
#       cancelRequest
#       Note: All methods just return Strings of simple text
#           Ex. Request < name > has been updated/closed/cancelled


class Request():
    def __init__(self, name, requester, dateRequested, status=''):
        self.name = name
        self.requester = requester
        self.dateRequested = dateRequested
        self.status = status
    #Methods
    def updateRequest(self):
        return(f"{self.name} has been updated")
    def closeRequest(self):
        return(f"{self.name} has been closed")
    def cancelRequest(self):
        return(f"{self.name} has been cancelled")
    def set_status(self, status):
        return(f"closed")

#Test cases
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Sales")
teamLead1 = teamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

employee1.getFullName()
admin1.getFullName()
teamLead1.getFullName()
employee2.login()
employee2.addRequest()
employee2.logout()
req2.closeRequest()
assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
# print(employee3.getFullName())
# print(employee4.getFullName())
print(req2.closeRequest())